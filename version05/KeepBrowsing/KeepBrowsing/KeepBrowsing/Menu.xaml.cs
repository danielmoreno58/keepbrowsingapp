﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KeepBrowsing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : MasterDetailPage
    {
        public Menu()
        {
            InitializeComponent();
            detalle();
        }

        //Metodo que hace la funcion del menu
        void detalle()
        {
            //Lista del menu
            List<detallemenu> menu = new List<detallemenu>
            {
                new detallemenu { page=new MainPage(), MenuTitle="Inicio"},
                new detallemenu { page=new QRLecturepage(), MenuTitle="QR"},
            };

            ListMenu.ItemsSource = menu; 

            Detail = new NavigationPage(new MainPage());
        }

        //Metodo para la seleccion de un elemento del menu
        void ListMenu_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var menu= e.SelectedItem as detallemenu;
            if (menu != null)
            {
                IsPresented = false;//codigo para ocultar barra cuando se selecciona
                Detail = new NavigationPage(menu.page);//codigo para mandarte a la pagina deseada con el parametro page
            }

        }

    }

    //Clase del detalle del menu
    public class detallemenu
    {
        //propiedades del menu
        public string MenuTitle { get; set; }
        public string MenuDetail { get; set; }
        public Page page { get; set; }
    }


}