﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KeepBrowsing
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void BtnSave_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new QRLecturepage());
        }

	}
}
